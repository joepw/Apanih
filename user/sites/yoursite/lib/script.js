
		function initMap() {
			var myLocation = {lat: -6.3646009, lng: 106.8286886};
			var destinationLocation = {lat: -6.3513753, lng: 106.7374029};
			
			var directionsService = new google.maps.DirectionsService;
			var directionsDisplay = new google.maps.DirectionsRenderer;
			
			var mapProp = {
				center: myLocation,
				zoom: 16,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}
			
			var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
			  
			
			var marker = new google.maps.Marker({
				position: myLocation,
				map: map,
				title: 'Fakultas Ilmu Komputer Universitas Indonesia, Depok'
			});
			
			directionsDisplay.setMap(map);
		
			findTheRoute(directionsService, directionsDisplay, myLocation, destinationLocation);
		  }
	  
		function findTheRoute(directionsService, directionsDisplay, myLocation, destinationLocation) {
			directionsService.route({
            origin: myLocation,
            destination: destinationLocation,
            travelMode: 'DRIVING'
			},
			function(response,status) {
            if(status === 'OK') {
              directionsDisplay.setDirections(response);
            }
            else {
              window.alert('Your request failed due to ' + status);
            }
          });
        }