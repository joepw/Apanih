{% extends "base.tpl" %}

{% block content %}

<div class="container-fluid bg-2 text-left">
	<h4> Self Catering Kitchen/Lounge </h4> 
	<br>
	<img src="/lib/images/self_catering.jpg" alt="self_catering" class="img-responsive" align="left">
	<p align="center">For guests who would prefer to cater for themselves we have a fully equipped kitchen with two microwave ovens, toasted sandwich makers, coffee maker, kettle, oven and fridge. The Lounge has a 52″ TV with DVD player and a library full of books, magazines and maps which are all available for our guests to browse through. The lounge has comfortable sofa and easy chairs which  you can snuggle up on in front of our wood burning stove on a cold winter’s evening.
	</p>
</div>
<div class="container-fluid bg-1 text-right">
  <h4 class="margin" style="color: white;">Conference Room</h4>
  <img src="/lib/images/conference_room.jpg" alt="conference_room" class="img-responsive" align="right" >
  <br><br>
  <p align="center">We have a large spacious room available for conferences/training/meetings, suitable for approximately 40 people and fitted with fridge, toilets, sink, kettle, crockery. Flip chart available.    Toilet facilities are available on the same level/floor.</p>
</div>
<div class="container-fluid bg-2 text-left">
	<h4> Outdoor Barbeque Area </h4> 
	<br>
	<img src="/lib/images/outdoor_barbeque.jpg" alt="outdoor_barbeque" class="img-responsive" align="left" >
	<br><br><br>
	<p align="center">We have a wonderful outdoor sitting area,with a brick Barbeque for our guests to use. Overlooking the surrounding lush fields, equipped with a table and surrounding benches it is the perfect setting on a beautiful summers day or for watching the sunset.
	</p>
</div>
<div class="container-fluid bg-4 text-center">
  <h4 style="color: white;">Other Facilities</h4>
  <br>
  	<li>Free WiFi Internet available</li>
  	<li>Free secure parking</li>
  	<li>Secure yard for bicycles</li>
  	<li>Drying room. Laundry facilities – drying room and clothes line</li>
  	<li>Disabled access: two downstairs rooms, with shower seats</li>
  	<li>Central Heating zoned to your needs</li>
  	<li>Wet room and large golf umbrellas available</li>
  	<li>You can also hire wet gear from us for an additional cost</li>
  	<li>Well equipped supermarket next door</li>
  	<li>Local Taxis available</li>
  	<li>Book of walking routes in each bedroom</li>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="script.js"></script>
<link rel="stylesheet" type="text/css" href="scriptcss.css" >
{% endblock %}
