{% extends "base.tpl" %}

{% block content %}

  {% wire id="buy" type="submit" postback={buy} delegate="controller_buy" %}
  <form id="buy" method="post" action="postback" class="form-horizontal">
  <div class="row bg-4 container-fluid">
  	<div class="col-md-8">
  		<div class="row">
			<div class="col-md-6">
			    <div class="thumbnail">
		        	<img src="/lib/images/nasi_goreng.png" alt="nasi_goreng" class="img-responsive" style="width:75%;height:150px;">
		        	<div class="caption">
		        		<label>Nasi Goreng</label>
						<div class="form-group">
						<label class="col-sm-6 control-label">Quantity</label>
						    <div class="col-sm-6">
						    	<input class="form-control" id="nasi_goreng" name="nasi_goreng" type="number" value="0">
						    </div>
						  </div>
						  <button class="btn btn-primary btn-block" type="submit">Send</button>
		        	</div>
			    </div>
			</div>
			<div class="col-md-6">
			    <div class="thumbnail">
		        	<img src="/lib/images/indomie_goreng.jpg" alt="Lights" class="img-responsive" style="width:75%;height:150px;">
		        	<div class="caption">
		        		<label>Indomie</label>
		          		<div class="form-group">
						    <label class="col-sm-6 control-label">Quantity</label>
						    <div class="col-sm-6">
						      	<input class="form-control" id="indomie" name="indomie" type="number" value="0">
						    </div>
						</div>
						<button class="btn btn-primary btn-block" type="submit">Send</button>
		        	</div>
			    </div>
			</div> 	
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="thumbnail">
				    <img src="/lib/images/kwetiaw.jpg" alt="Lights" class="img-responsive" style="width:75%;height:150px;">
				    <div class="caption">
				       	<label>Kwetiaw</label>
					    <div class="form-group">
							<label class="col-sm-6 control-label">Quantity</label>
							<div class="col-sm-6">
								<input class="form-control" id="kwetiaw" name="kwetiaw" type="number" value="0">
							</div>
						</div>
						<button class="btn btn-primary btn-block" type="submit">Send</button>
				    </div>
				</div>
			</div>
			<div class="col-md-6">
			    <div class="thumbnail">
		        	<img src="/lib/images/bihun_goreng.jpg" alt="nasi_goreng" class="img-responsive" style="width:75%;height:150px;">
		        	<div class="caption">
		        		<label>Bihun Goreng</label>
						<div class="form-group">
						    <label class="col-sm-6 control-label">Quantity</label>
						    <div class="col-sm-6">
						      	<input class="form-control" id="bihun_goreng" name="bihun_goreng" type="number" value="0">
						    </div>
						</div>
						<button class="btn btn-primary btn-block" type="submit">Send</button>
		        	</div>
			    </div>
			</div>
		</div>
    </form>
	</div>
	<div id="sidebar" class="col-md-4">
		{% wire id="buy_online" type="submit" postback={buy_online} delegate="controller_buy_online" %}
		<form id='buy_online' method='post' action='postback' class='form-horizontal'>
			<div id="hasil"></div>
		</form>
	</div>	  
</div>
	
	

{% endblock %}
