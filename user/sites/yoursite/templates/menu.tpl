{% extends "base.tpl" %}

{% block content %}
    <div class="row bg-4 container-fluid">
    	<!-- <div class="form-group" style="margin-top:40px">
		  <select id="menuselector" class="form-control" id="sel1" autocomplete="off">
		    <option selected>Select Restaurant:</option>
		    <option value="ali">ALI BABA restaurant</option>
		    <option>KFC</option>
		    <option>Ketoprak Jak</option>
		  </select>
		</div> -->

		<div id="ali" class="menu" style="text-align: center;">
    		<h1 style="color: white;">Serving the perfect blend between taste, service, and price.</h1>
			<p style="text-align: center;"><strong>Express Lunch Buffet Monday - Saturday &nbsp;&nbsp; 11am - 2pm</strong></p>
			<h2 style="color: white">Foods</h2>
				<ul>
					<div class="checkbox">
							        
		        		<img src="/lib/images/nasi_goreng.png" alt="nasi_goreng" class="img-responsive" style="width:250px;height:150px; margin: auto;">
		        		<br>
						<label><input id="nasi_goreng" type="checkbox" value="1" onchange="valueChanged()"> Nasi Goreng - Rp 25.000</label>
						<br><br>
					  	<div id="input_div1" style="display:none;margin-bottom:10px">
						    <input type="text" size="25" value="1" id="count" style="color: black">
						    <input type="button" value="-" id="moins" style="color: black" onclick="minus()">
						    <input type="button" value="+" id="plus" style="color: black" onclick="plus()">
						</div>				
					</div>
					<div class="checkbox">
						<img src="/lib/images/indomie_goreng.jpg" alt="Lights" class="img-responsive" style="width:250px; height:150px; margin: auto;">
						<br>
					  	<label><input id="indomie_goreng" type="checkbox" value="2" onchange="valueChanged()"> Indomie - Rp 15.000</label>
					  	<br><br>
					  	<div id="input_div2" style="display:none;margin-bottom:10px">
						    <input type="text" size="25" value="1" id="count1" style="color: black">
						    <input type="button" value="-" id="moins" style="color: black" onclick="minus1()">
						    <input type="button" value="+" id="plus" style="color: black" onclick="plus1()">
						</div>
					</div>
					<div class="checkbox">
					    <img src="/lib/images/kwetiaw.jpg" alt="Lights" class="img-responsive" style="width:250px;height:150px; margin: auto;">
					    <br>

					  <label><input id="kwetiaw" type="checkbox" value="3" onchange="valueChanged()"> Kwetiaw - Rp 20.000</label>
					  <br><br>
					  <div id="input_div3" style="display:none;margin-bottom:10px">
						    <input type="text" size="25" value="1" id="count2" style="color: black">
						    <input type="button" value="-" id="moins" style="color: black" onclick="minus2()">
						    <input type="button" value="+" id="plus" style="color: black" onclick="plus2()">
						</div>
					</div>
					<div class="checkbox">
							        <img src="/lib/images/bihun_goreng.jpg" alt="nasi_goreng" class="img-responsive" style="width:250px;height:150px; margin: auto;">
							        <br>

					  <label><input id="bihun_goreng" type="checkbox" value="4" onchange="valueChanged()"> Bihun Goreng - Rp 25.000</label>
					  <br><br>
					  <div id="input_div4" style="display:none;margin-bottom:10px">
						    <input type="text" size="25" value="1" id="count3" style="color: black">
						    <input type="button" value="-" id="moins" style="color: black" onclick="minus3()">
						    <input type="button" value="+" id="plus" style="color: black" onclick="plus3()">
						</div>
					</div>
				</ul>
			<!-- <button type="button" class="btn btn-primary">Add To Cart</button> -->
			{% button text="Order Now" class="btn btn-primary" action={redirect location="/buy_online?"} %}
	    </div>
	    <div id="ala" class="menu" style="display:none">
	    	<p>par2</p>
	    </div> 
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script
			  src="https://code.jquery.com/jquery-3.2.1.js"
			  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
			  crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script> 

    <!-- menu selection jquery-->
    <script type="text/javascript">
	    	$(function() {
		        $('#menuselector').change(function(){
		            $('.menu').hide();
		            $('#' + $(this).val()).show();
		        });
		    });
    </script>
    <script type="text/javascript">
		function valueChanged(){
		    if($('#nasi_goreng').is(":checked"))   
		        $("#input_div1").show();
		    else
		        $("#input_div1").hide();

		    if($('#indomie_goreng').is(":checked"))   
		        $("#input_div2").show();
		    else
		        $("#input_div2").hide();

		    if($('#kwetiaw').is(":checked"))   
		        $("#input_div3").show();
		    else
		        $("#input_div3").hide();

		    if($('#bihun_goreng').is(":checked"))   
		        $("#input_div4").show();
		    else
		        $("#input_div4").hide();
		}


	    var count = 1;
	    var countEl = document.getElementById("count");
	    function plus(){
	        count++;
	        countEl.value = count;
	    }
	    function minus(){
	      if (count > 1) {
	        count--;
	        countEl.value = count;
	      }  
	    }

	    var count1 = 1;
	    var countEl1 = document.getElementById("count1");
	    function plus1(){
	        count1++;
	        countEl1.value = count1;
	    }
	    function minus1(){
	      if (count1 > 1) {
	        count1--;
	        countEl1.value = count1;
	      }  
	    }

	    var count2 = 1;
	    var countEl2 = document.getElementById("count2");
	    function plus2(){
	        count2++;
	        countEl2.value = count2;
	    }
	    function minus2(){
	      if (count2 > 1) {
	        count2--;
	        countEl2.value = count2;
	      }  
	    }

	    var count3 = 1;
	    var countEl3 = document.getElementById("count3");
	    function plus3(){
	        count3++;
	        countEl3.value = count3;
	    }
	    function minus3(){
	      if (count3 > 1) {
	        count3--;
	        countEl3.value = count3;
	      }  
	    }
	</script>
{% endblock %}
