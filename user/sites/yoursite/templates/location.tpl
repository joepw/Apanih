{% extends "base.tpl" %}

{% block content %}

<div id="first" class="container-fluid bg-1 text-center">
  <h3 class="margin">Want to know our stores?</h3>
	<iframe src="https://www.google.com/maps/d/embed?mid=1kEzJ-3ZMc3PbhU5IgZV8D6wpm9gsWfOe" width="640" height="480"></iframe>
 <h3>Let us go!</h3>
 <br>
		 <h4> Information about our locations: </h4>
		 <h5> <a href="http://www.cs.ui.ac.id">Fakultas Ilmu Komputer, Univ. Indonesia</a>- First Store</h5>
		 <h5> <a href="http://www.fk.ui.ac.id">Pusat Administrasi, Univ. Indonesia</a>- Second Store</h5>
		 <h5> <a href="http://www.law.ui.ac.id">Fakultas Hukum, Univ. Indonesia</a>- Third Store</h5>
		<br>
		 <p> ---------------------------------------- </p>
		<form action="">
		  give us recommendation about a good location for our new store<br>
		  <input type="text" name="recommendation"><br>
		  <input type="submit" value="Submit">
		</form>

</div>
<div class="container-fluid bg-2 text-center">
  <h3 class="margin">Your Way</h3>
  <div id="googleMap" class="container text-center" style="width:500px;height:380px;"></div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="script.js"></script>
<link rel="stylesheet" type="text/css" href="scriptcss.css" >
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD0iBAfZ6Elgg6Sdav-2NWSwVzO-ypLXyU"></script>
<script async defer src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD0iBAfZ6Elgg6Sdav-2NWSwVzO-ypLXyU&callback=initMap"></script>
{% endblock %}
