%% @author author <author@example.com>
%% @copyright YYYY author.
%% @doc Example buy-form handler.

-module(controller_buy).
-export([event/2]).

-include_lib("zotonic.hrl").

event(#submit{message={buy, []}}, Context) ->
    Nasi_goreng = z_context:get_q("nasi_goreng", Context),
    Indomie = z_context:get_q("indomie", Context),
    Kwetiaw = z_context:get_q("kwetiaw", Context),
    Bihun_goreng = z_context:get_q("bihun_goreng", Context),
    if Nasi_goreng =:= "0" andalso Indomie =:= "0" andalso Kwetiaw =:= "0" andalso Bihun_goreng =:= "0" ->
	    z_render:appear("hasil", "<p>Please choose one of the menu</p>", Context);
	    true -> z_render:appear("hasil", "
	    	<form id='buy_online' method='post' action='postback' class='form-horizontal'>
		    	<div class='form-group'>
				    <label class='col-sm-6 control-label'>Nasi Goreng</label>
				    <div class='col-sm-6'>
				      <input class='form-control' id='nasi_goreng' name='nasi_goreng' type='number' value=" ++ Nasi_goreng ++ " disabled>
				    </div>
				  </div>
				    <div class='form-group'>
				    <label class='col-sm-6 control-label'>Indomie</label>
				    <div class='col-sm-6'>
				      <input class='form-control' id='indomie' name='indomie' type='number' value=" ++ Indomie ++ " disabled>
				    </div>
				 	</div>
				    <div class='form-group'>
				    <label class='col-sm-6 control-label'>Kwetiaw</label>
				    <div class='col-sm-6'>
				      <input class='form-control' id='kwetiaw' name='kwetiaw' type='number' value=" ++ Kwetiaw ++ " disabled>
				    </div>
				  </div>
				    <div class='form-group'>
				    <label class='col-sm-6 control-label'>Bihun Goreng</label>
				    <div class='col-sm-6'>
				      <input class='form-control' id='bihun_goreng' name='bihun_goreng' type='number' value=" ++ Bihun_goreng ++ " disabled>
				    </div>
				  </div>
				  <div class='form-group'>
				  	<div class='col-sm-12'>
				  		<button class='btn btn-primary btn-block' type='submit'>Checkout</button>
				  	</div>
				  </div>
		    </form>", Context)
  	end.