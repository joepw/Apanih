%% @author author <author@example.com>
%% @copyright YYYY author.
%% @doc Example buy-online-form handler.

-module(controller_buy_online).
-export([event/2]).

-include_lib("zotonic.hrl").

event(#submit{message={buy_online, []}}, Context) ->
    Vars = [{nasi_goreng, z_context:get_q("nasi_goreng", Context)},
            {indomie, z_context:get_q("indomie", Context)},
            {kwetiaw, z_context:get_q("kwetiaw", Context)},
            {bihun_goreng, z_context:get_q("bihun_goreng", Context)}],
    z_render:appear("hasil", "<p>The order has been submitted, please wait for your order, Thankyou</p>", Context).